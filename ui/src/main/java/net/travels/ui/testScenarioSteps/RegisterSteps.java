package net.travels.ui.testScenarioSteps;

import io.qameta.allure.Step;
import net.travels.ui.pageClasses.CustomerData;
import net.travels.ui.pageClasses.MainPage;
import net.travels.ui.pageClasses.RegisterPage;

public class RegisterSteps {

  private MainPage mainPage = new MainPage();
  private RegisterPage registerPage = new RegisterPage();

  @Step("Register user with valid data with different languages")
  public CustomerData registerUserWithValidData() {
    mainPage.selectLanguageInDropdown("en");
    mainPage.navigateToSignUpPage();

    CustomerData customerData = new CustomerData();
    registerPage
            .setValuesInRegisterFields(customerData)
            .clickOnSignUpButton();
    return customerData;
  }
}
