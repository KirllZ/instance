package net.travels.ui.dataProvider;

import org.testng.annotations.DataProvider;

public class ValidationEmailDataProvider {

  @DataProvider(name = "Invalid emails")
  public static Object[][] invalidEmails() {
    return new Object[][]{
            {"Abc.example.com"},
            {"A@b@c@example.com"},
            {"1234567890123456789012345678901234567890123456789012345678901234+x@example.com"},
            {"john..doe@example.com"},
            {"john.doe@example..com"},
            {"john.doe@example com"},
            {"john doe@example.com"}
    };
  }
}
