package net.travels.ui.pageClasses;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import lombok.Getter;
import lombok.experimental.Accessors;
import static com.codeborne.selenide.Selenide.$;

@Getter
@Accessors(fluent = true)
public class MainPage extends BasePage{

  SelenideElement languageDropdown = $("a[id='dropdownLangauge'");
  SelenideElement languagesList = $("div[aria-labelledby='dropdownLangauge']");

  @Step("Select language in dropdown")
  public void selectLanguageInDropdown(String lang) {
    languageDropdown.click();
    languagesList.$("#" + lang).click();
  }

  @Step("Navigate to 'Sign Up' page")
  public void navigateToSignUpPage() {
    myAccountDropdown.click();
    myAccountSignUp.click();
  }
}
