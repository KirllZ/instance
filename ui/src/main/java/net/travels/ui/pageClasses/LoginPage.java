package net.travels.ui.pageClasses;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class LoginPage extends BasePage{

  private SelenideElement goHome = $("li[class='go-right text-center']");

  @Step("Click to home page")
  public void clickToHomePage() {
    goHome.click();
  }
}
