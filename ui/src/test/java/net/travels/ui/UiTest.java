package net.travels.ui;

import net.travels.ui.dataProvider.DataProviderLanguage;
import net.travels.ui.dataProvider.ValidationEmailDataProvider;
import net.travels.ui.pageClasses.*;
import net.travels.ui.testScenarioSteps.RegisterSteps;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static com.codeborne.selenide.Selenide.*;
import static net.travels.ui.dataGenerator.UserDataGenerator.getFakerRandomNumber;
import static net.travels.ui.helpers.properties.UrlLinksProp.MAIN_URL;

public class UiTest extends BaseTest {

  private RegisterPage registerPage = new RegisterPage();
  private MainPage mainPage = new MainPage();
  private AccountPage accountPage = new AccountPage();
  private LoginPage loginPage = new LoginPage();
  private RegisterSteps registerSteps = new RegisterSteps();

  @BeforeMethod
  public void openMainPage(){
    open(MAIN_URL);
    clearBrowserCookies();
  }

  @Test(description = "Register user with valid data with different languages",
           dataProvider = "Language", dataProviderClass = DataProviderLanguage.class)
  public void registerUserWithValidData(String lang){
    mainPage.selectLanguageInDropdown(lang);
    mainPage.navigateToSignUpPage();

    CustomerData customerData = new CustomerData();
    registerPage
       .setValuesInRegisterFields(customerData)
       .clickOnSignUpButton();

    // verify that user is registered checking his email in 'My Profile'
    accountPage.checkEmail(customerData.getEmail());
  }

  @Test(description = "Register user with existed email",
          dataProvider = "Language", dataProviderClass = DataProviderLanguage.class)
  public void registerUserWithExistedEmail(String lang){
    // Create customer and get email
    CustomerData customerData = registerSteps.registerUserWithValidData();
    sleep(5000);
    accountPage.clickLogout();
    sleep(5000);

    mainPage.selectLanguageInDropdown(lang);
    mainPage.navigateToSignUpPage();

    registerPage
            .setValuesInRegisterFields(new CustomerData().setEmail(customerData.getEmail()))
            .clickOnSignUpButton();

    registerPage.checkAlertMessage("Email Already Exists.");
  }

  @Test(description = "Register user with empty form",
          dataProvider = "Language", dataProviderClass = DataProviderLanguage.class)
  public void registerUserWithEmptyForm(String lang){
    mainPage.selectLanguageInDropdown(lang);
    mainPage.navigateToSignUpPage();

    registerPage.clickOnSignUpButton();

    registerPage.checkAlertMessage("The Email field is required." + "\n" + "The Password field is required." + "\n" +
            "The Password field is required." + "\n" + "The First name field is required." + "\n" + "The Last Name field is required.");
  }

  @Test(description = "Register user without first name",
           dataProvider = "Language", dataProviderClass = DataProviderLanguage.class)
  public void registerUserWithoutFirstName(String lang){
    mainPage.selectLanguageInDropdown(lang);
    mainPage.navigateToSignUpPage();

    CustomerData customerData = new CustomerData()
            .setFirstName(null);
    registerPage
            .setValuesInRegisterFields(customerData)
            .clickOnSignUpButton();

    registerPage.checkAlertMessage("The First name field is required.");
  }

  @Test(description = "Register user without last name",
          dataProvider = "Language", dataProviderClass = DataProviderLanguage.class)
  public void registerUserWithoutLastName(String lang){
    mainPage.selectLanguageInDropdown(lang);
    mainPage.navigateToSignUpPage();

    CustomerData customerData = new CustomerData()
            .setLastName(null);
    registerPage
            .setValuesInRegisterFields(customerData)
            .clickOnSignUpButton();

    registerPage.checkAlertMessage("The Last Name field is required.");
  }

  @Test(description = "Register user without email",
          dataProvider = "Language", dataProviderClass = DataProviderLanguage.class)
  public void registerUserWithoutEmail(String lang){
    mainPage.selectLanguageInDropdown(lang);
    mainPage.navigateToSignUpPage();

    CustomerData customerData = new CustomerData()
            .setEmail(null);
    registerPage
            .setValuesInRegisterFields(customerData)
            .clickOnSignUpButton();

    registerPage.checkAlertMessage("The Email field is required.");
  }

  @Test(description = "Register user without password",
          dataProvider = "Language", dataProviderClass = DataProviderLanguage.class)
  public void registerUserWithoutPassword(String lang){
    mainPage.selectLanguageInDropdown(lang);
    mainPage.navigateToSignUpPage();

    CustomerData customerData = new CustomerData()
            .setPassword(null);
    registerPage
            .setValuesInRegisterFields(customerData)
            .clickOnSignUpButton();

    registerPage.checkAlertMessage("The Password field is required.\n" + "\n" + "Password not matching with confirm password.");
  }

  @Test(description = "Register user without confirm password",
          dataProvider = "Language", dataProviderClass = DataProviderLanguage.class)
  public void registerUserWithoutConfirmPassword(String lang){
    mainPage.selectLanguageInDropdown(lang);
    mainPage.navigateToSignUpPage();

    CustomerData customerData = new CustomerData()
            .setPassword(null);
    registerPage
            .setValuesInRegisterFields(customerData)
            .clickOnSignUpButton();

    registerPage.checkAlertMessage("The Password field is required.\n" + "\n" + "Password not matching with confirm password.");
  }

  @Test(description = "Register user with not matching password",
          dataProvider = "Language", dataProviderClass = DataProviderLanguage.class)
  public void registerUserWithNotMatchingPassword(String lang){
    mainPage.selectLanguageInDropdown(lang);
    mainPage.navigateToSignUpPage();

    CustomerData customerData = new CustomerData()
            .setPassword(getFakerRandomNumber(8))
            .setConfirmPassword(getFakerRandomNumber(8));
    registerPage
            .setValuesInRegisterFields(customerData)
            .clickOnSignUpButton();

    registerPage.checkAlertMessage("Password not matching with confirm password.");
  }

  @Test(description = "Register user with less 6 characters password",
          dataProvider = "Language", dataProviderClass = DataProviderLanguage.class)
  public void registerUserWithLess6charactersPassword(String lang){
    mainPage.selectLanguageInDropdown(lang);
    mainPage.navigateToSignUpPage();

    registerPage
            .setValuesInRegisterFields(new CustomerData().setPassword("12345").setConfirmPassword("12345"))
            .clickOnSignUpButton();

    registerPage.checkAlertMessage("The Password field must be at least 6 characters in length.");
  }

  @Test(description = "Register user with wrong email",
          dataProvider = "Invalid emails", dataProviderClass = ValidationEmailDataProvider.class)
  public void registerUserWithWrongEmail(String email){
    mainPage.selectLanguageInDropdown("en");
    mainPage.navigateToSignUpPage();

    registerPage
            .setValuesInRegisterFields(new CustomerData().setEmail(email))
            .clickOnSignUpButton();

    registerPage.checkAlertMessage("The Email field must contain a valid email address.");
  }
}
